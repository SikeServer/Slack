package com.sikeserver.slack.listener;

import com.sikeserver.slack.object.SlackMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.sikeserver.slack.util.Slack;

public class PlayerJoinListener implements Listener {
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
        Slack.post(
            new SlackMessage(
                event.getPlayer().getName() + "が接続しました。", "#chat"
            )
        );
	}
}
