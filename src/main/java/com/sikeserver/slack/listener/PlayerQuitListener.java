package com.sikeserver.slack.listener;

import com.sikeserver.slack.object.SlackMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sikeserver.slack.util.Slack;

public class PlayerQuitListener implements Listener {
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Slack.post(
		    new SlackMessage(
		        event.getPlayer().getName() + "が切断しました。", "#chat"
            )
        );
	}
}
