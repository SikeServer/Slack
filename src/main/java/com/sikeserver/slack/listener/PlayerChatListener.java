package com.sikeserver.slack.listener;

import java.net.MalformedURLException;
import java.net.URL;

import com.sikeserver.slack.object.SlackMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.sikeserver.slack.util.Slack;

public class PlayerChatListener implements Listener {
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		try {
			SlackMessage msg = new SlackMessage(
				event.getMessage(),
				event.getPlayer().getName(),
				new URL("https://minotar.net/helm/" + event.getPlayer().getName()),
				"#chat"
			);
			Slack.post(msg);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
}
