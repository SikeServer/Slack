package com.sikeserver.slack.util;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sikeserver.slack.SlackManager;
import com.sikeserver.slack.object.SlackMessage;
import org.bukkit.scheduler.BukkitRunnable;

public class Slack {
	public static void post(SlackMessage msg) {
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					URL url = new URL("https://hooks.slack.com/services/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");
					HttpURLConnection con = (HttpURLConnection) url.openConnection();
					con.setRequestMethod("POST");
					con.setConnectTimeout(5000);
					con.setUseCaches(false);
					con.setDoInput(true);
					con.setDoOutput(true);
					
					DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					wr.writeBytes(
						"payload=" + URLEncoder.encode(new ObjectMapper().writeValueAsString(msg), "UTF-8")
					);
					wr.flush();
					wr.close();
					
					con.getInputStream().close();;
					con.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.runTaskAsynchronously(SlackManager.getPlugin());
	}
}
