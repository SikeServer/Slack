package com.sikeserver.slack.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import static java.nio.file.StandardWatchEventKinds.*;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.sikeserver.slack.SlackManager;

public class FileWatcher extends BukkitRunnable {
	private String dirName;

	public FileWatcher(File dir) {
		dirName = dir.getAbsolutePath();
	}

	@Override
	public void run() {
		Path dir = Paths.get(dirName);
        WatchService watcher;
		try {
			watcher = FileSystems.getDefault().newWatchService();
			dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

			for (;;) {
				WatchKey key = watcher.take();
				for (WatchEvent<?> event: key.pollEvents()) {
					if (event.kind() == OVERFLOW) continue;
					
					Thread.sleep(50);
					
					try {
						FileReader fr = new FileReader(dirName + "/message.txt");
						BufferedReader br = new BufferedReader(fr);
						
						String line;
						while ((line = br.readLine()) != null) {
							String[] array = line.split(",", 2);
							SlackManager.getPlugin().getServer().broadcastMessage(
								"<" + ChatColor.BOLD + "["
									+ ChatColor.LIGHT_PURPLE + ChatColor.BOLD + "S"
									+ ChatColor.AQUA + ChatColor.BOLD + "l"
									+ ChatColor.GREEN + ChatColor.BOLD + "a"
									+ ChatColor.YELLOW + ChatColor.BOLD + "c"
									+ ChatColor.DARK_PURPLE + ChatColor.BOLD + "k"
									+ ChatColor.RESET + ChatColor.BOLD + "] "
									+ array[0] + "> " + ChatColor.RESET + array[1]
							);
						}
						
						br.close();
						fr.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}

	@SuppressWarnings("unchecked")
	static <T> WatchEvent<T> cast(WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}
}
