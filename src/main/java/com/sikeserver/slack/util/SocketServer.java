package com.sikeserver.slack.util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.sikeserver.slack.SlackManager;

public class SocketServer extends BukkitRunnable {
	@Override
	public void run() {
		ExecutorService worker = Executors.newCachedThreadPool();
		try (ServerSocketChannel listener = ServerSocketChannel.open();) {
		    listener.setOption(StandardSocketOptions.SO_REUSEADDR, Boolean.TRUE);
		    listener.bind(new InetSocketAddress(15243));
		    System.out.println("Server listening on port 15243...");
		    while (true) {
		        final SocketChannel _channel = listener.accept();
		        System.out.printf("ACCEPT %s%n", _channel);
		        worker.submit(() -> {
                    try (SocketChannel channel = _channel;) {
                        String[] array = Bytes.getString(channel).split(":", 2);
                        SlackManager.getPlugin().getServer().broadcastMessage(
                            "<" + ChatColor.BOLD + "["
                                + ChatColor.LIGHT_PURPLE + ChatColor.BOLD + "S"
                                + ChatColor.AQUA + ChatColor.BOLD + "l"
                                + ChatColor.GREEN + ChatColor.BOLD + "a"
                                + ChatColor.YELLOW + ChatColor.BOLD + "c"
                                + ChatColor.DARK_PURPLE + ChatColor.BOLD + "k"
                                + ChatColor.RESET + ChatColor.BOLD + "] "
                                + array[0] + "> " + ChatColor.RESET + array[1]
                        );
                        System.out.printf("CLOSE %s%n", channel);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    worker.shutdown();
		}
	}
}
