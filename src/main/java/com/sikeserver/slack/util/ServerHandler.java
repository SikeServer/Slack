package com.sikeserver.slack.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bukkit.ChatColor;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.sikeserver.slack.SlackManager;

public class ServerHandler extends AbstractHandler {
	@Override
	public void handle(String arg0, Request request, HttpServletRequest arg2, HttpServletResponse arg3)
			throws IOException, ServletException {
		if (request.getMethod().equals("POST")) {
			if (request.getParameter("user_name").equals("slackbot")) return;
			
            SlackManager.getPlugin().getServer().broadcastMessage(
				"<" + ChatColor.BOLD + "["
					+ ChatColor.LIGHT_PURPLE + ChatColor.BOLD + "S"
					+ ChatColor.AQUA + ChatColor.BOLD + "l"
					+ ChatColor.GREEN + ChatColor.BOLD + "a"
					+ ChatColor.YELLOW + ChatColor.BOLD + "c"
					+ ChatColor.DARK_PURPLE + ChatColor.BOLD + "k"
					+ ChatColor.RESET + ChatColor.BOLD + "] "
					+ ChatColor.RESET
					+ request.getParameter("user_name") + "> "
					+ ChatColor.RESET
					+ ChatColor.translateAlternateColorCodes('&',
						request.getParameter("text").replaceAll("&amp;", "&")
					  )
			);
        }
	}
}
