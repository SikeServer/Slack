package com.sikeserver.slack.util;

import java.util.logging.Level;

import org.bukkit.scheduler.BukkitRunnable;
import org.eclipse.jetty.server.Server;

import com.sikeserver.slack.SlackManager;
import org.eclipse.jetty.util.log.Log;

public class HttpServer extends BukkitRunnable {
	private SlackManager plugin;
	
	private Server server;
	
	public HttpServer() {
		plugin = SlackManager.getPlugin();
	}
	
	@Override
	public void run() {
        Log.setLog(new NoLogging());
        
        server = new Server(8080);
		server.setHandler(new ServerHandler());
	    try {
	        server.start();
	        server.join();
	    } catch (Exception e) {
	        plugin.getLogger().log(Level.SEVERE, "Error in starting SlackMC server: " + e);
	    }
	}
	
	public void close() {
		try {
			server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}