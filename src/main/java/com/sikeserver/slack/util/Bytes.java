package com.sikeserver.slack.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class Bytes {
    private static final int BUF_SIZE = 0x1000;

    public static String getString(ReadableByteChannel channel) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(BUF_SIZE);
        while (channel.read(buf) >= 0 || buf.position() != 0) {
            buf.flip();
            buf.compact();
        }
        return new String(buf.array());
    }
}