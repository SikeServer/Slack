package com.sikeserver.slack;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.slack.listener.*;
import com.sikeserver.slack.util.HttpServer;

public class SlackManager extends JavaPlugin {
	public static SlackManager plugin;
	public HttpServer server;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		plugin = this;
		
		PluginManager manager = Bukkit.getServer().getPluginManager();
		manager.registerEvents(new PlayerJoinListener(), this);
		manager.registerEvents(new PlayerQuitListener(), this);
		manager.registerEvents(new PlayerChatListener(), this);
		
		server = new HttpServer();
		getServer().getScheduler().runTaskAsynchronously(this, server);
		
		//Slack.sendMessage("サーバーを起動しました。");
	}
	
	@Override
	public void onDisable() {
		server.close();
		
		//Slack.sendMessage("サーバーを停止しました。");
	}
	
	public static SlackManager getPlugin() {
		return plugin;
	}
}
