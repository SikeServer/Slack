package com.sikeserver.slack.object;

import java.net.URL;

public class SlackMessage {
    public String text;
    public String username;
    public String icon_url;
    public String icon_emoji;
    public String channel;
    
    public SlackMessage(String msg) {
        this(msg, "Information", ":loudspeaker:");
    }
    
    public SlackMessage(String msg, String channel) {
        this(msg, "Information", ":loudspeaker:", channel);
    }
    
    public SlackMessage(String msg, String name, URL image, String channel) {
        this.text = msg;
        this.username = name;
        this.icon_url = image.toString();
        this.channel = channel;
    }
    
    public SlackMessage(String msg, String name, String emoji) {
        this.text = msg;
        this.username = name;
        this.icon_emoji = emoji;
    }
    
    public SlackMessage(String msg, String name, String emoji, String channel) {
        this.text = msg;
        this.username = name;
        this.icon_emoji = emoji;
        this.channel = channel;
    }
}